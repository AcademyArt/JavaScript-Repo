/**
 * Copyright (c) 2018
 *
 * This is where the global javascript functions will be kept for use in the website.
 *
 * @summary How to filter what is on a webpage... both in JQuery and Vanilla
 * @author Andrew Pankow <apankow1234@gmail.com>
 */

function scroll_to(ClassOrIdToScrollTo) {
	// smooth scroll if CSS:  html{scroll-behavior: smooth;}
	let placeOnPage = document.querySelector(ClassOrIdToScrollTo).offsetTop;
	let menuHeight = document.getElementsByTagName("header")[0].clientHeight;
	document.getElementsByTagName("html")[0].scrollTop = placeOnPage - menuHeight;
}