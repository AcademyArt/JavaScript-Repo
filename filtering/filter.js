/**
 * Copyright (c) 2018
 *
 * This is where the global javascript functions will be kept for use in the website.
 *
 * @summary How to filter what is on a webpage... both in JQuery and Vanilla
 * @author Andrew Pankow <apankow1234@gmail.com>
 */

/* JQuery Version */
$(function() {
	var searchBarClass = ".filter_search_bar";
	var itemClass      = ".show_item";
	var hidingClass    = "hidden";
	$( "body" )	
	.on( "input", searchBarClass, function() {
		var regex = RegExp( $(this).val(), 'i' );
		$( itemClass ).each( function() {
			let isBeingHidden = $(this).hasClass( hidingClass );
			if( regex.test( $( this ).text() ) ) {
				if( isBeingHidden ) $(this).removeClass( hidingClass );
			} else {
				if( !isBeingHidden ) $(this).addClass( hidingClass );
			}
		} )
	} )
} );


/* Regular JavaScript (ECMA6) Version */
window.onload = function() {
	var searchBarClass = "filter_search_bar";
	var itemClass      = "show_item"
	var hidingClass    = "hidden";
	function test() {
		var regex = RegExp( searchBar.value, 'i' );
		var itemsToShow = document.getElementsByClassName(itemClass);
		for( var i=0; i<itemsToShow.length; i++ ) {
			let isBeingHidden = Object.values(itemsToShow[i].classList).includes(hidingClass); 
			if( regex.test( itemsToShow[i].innerText ) ) {
				if( isBeingHidden ) itemsToShow[i].classList.remove( hidingClass );
			} else {
				if( !isBeingHidden ) itemsToShow[i].classList.add( hidingClass );
			}
		}
	}
	var searchBar = document.getElementsByClassName(searchBarClass)[0];
	searchBar.addEventListener( "input", test );
};